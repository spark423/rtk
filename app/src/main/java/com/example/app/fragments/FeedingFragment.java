package com.example.app.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app.R;
import com.example.app.activities.AttendanceActivity;
import com.example.app.activities.FeedingActivity;
import com.example.app.adapters.ClassListAdapter;
import com.example.app.adapters.SchoolFeedingAdapter;
import com.example.app.asynctasks.HttpGetRequests;
import com.example.app.interfaces.CallbackListener;
import com.example.app.interfaces.ClickListener;
import com.example.app.models.Calendar;
import com.example.app.models.Food;
import com.example.app.models.School;
import com.example.app.util.ChildViewClickListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

//import org.parceler.Parcels;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;

import static com.example.app.util.Constants.GET_DAILY_ATTENDANCE;
import static com.example.app.util.Constants.GET_DAILY_FOOD;
import static com.example.app.util.Constants.GET_MONTHLY_ATTENDANCE;
import static com.example.app.util.Constants.GET_MONTHLY_FOOD;
import static com.example.app.util.Constants.REQUEST_FOOD_INFO;
import static com.example.app.util.DateUtils.setOnlyDate;

public class FeedingFragment extends Fragment implements ClickListener {

    private ArrayList<Calendar> mSchoolCalendars;
    private SchoolFeedingAdapter mAdapter;
    //private Date mSelecteddate;

    public static FeedingFragment newInstance (ArrayList<Calendar> calendars){
        FeedingFragment fragment = new FeedingFragment();
        Bundle bundle = new Bundle();
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<Calendar>>() {}.getType();
        String listofcalendars = gson.toJson(calendars, type);
        bundle.putString("SchoolCalendar", listofcalendars);
        //bundle.putParcelable("SchoolCalendar", Parcels.wrap(calendars));
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Gson gson = new Gson();
            String calendarlist = getArguments().getString("SchoolCalendar");
            if (calendarlist != null) {
                Type type = new TypeToken<ArrayList<Calendar>>() {}.getType();
                mSchoolCalendars = gson.fromJson(calendarlist, type);
            } else {
                Log.v("Exception", "Could not retrieve this school's feeding calendar");
            }
            //mSchoolCalendars = Parcels.unwrap(getArguments().getParcelable("SchoolCalendar"));
        }
        //add get request for retrieving feeding calendar from db
        Log.v("test2", "testing this fragment");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feeding, container, false);
        RecyclerView mDatelistview = view.findViewById(R.id.dateList);
        mDatelistview.setFocusable(true);
        mAdapter = new SchoolFeedingAdapter(getContext(), mSchoolCalendars);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mDatelistview.setLayoutManager(mLayoutManager);
        mDatelistview.setAdapter(mAdapter);
        mDatelistview.addOnItemTouchListener(new ChildViewClickListener(getContext(), mDatelistview, this));
        return view;
    }

    @Override
    public void onClick(View v, final int position) {
        final TextView datename = v.findViewById(R.id.class_name);
        datename.setFocusable(true);
        datename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.requestFocus();
                Calendar selectedDate = mAdapter.getSelectedDate(position);

                Log.v("selectedDate", selectedDate.getFirst().toString());
                ArrayList<Food> foodServed = (ArrayList<Food>) selectedDate.getSecond();
                Log.v("feedingfragfoodlist", foodServed.toString());
                Intent intent = new Intent(getContext(), FeedingActivity.class);
                Gson gson = new Gson();
                Type type = new TypeToken<ArrayList<Food>>() {}.getType();
                String foodlist = gson.toJson(foodServed, type);
                intent.putExtra("selectedfood", foodlist);
                intent.putExtra("selecteddate", selectedDate.getFirst().toString());
                //intent.putExtra("schoolname", )
                startActivity(intent);
            }
        });
    }
}
