package com.example.app.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.app.R;
import com.example.app.asynctasks.HttpGetRequests;
import com.example.app.asynctasks.HttpPostRequests;
import com.example.app.fragments.SuccessDialog;
import com.example.app.interfaces.CallbackListener;
import com.example.app.models.Class;
import com.example.app.models.School;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static com.example.app.util.Constants.GET_CLASSES;
import static com.example.app.util.Constants.GET_SCHOOLS;
import static com.example.app.util.Constants.POST_NEW_USER;
import static com.example.app.util.Constants.REQUEST_CLASS_LIST;
import static com.example.app.util.Constants.REQUEST_ORG_DASHBOARD;
import static com.example.app.util.Constants.REQUEST_REGISTER_USER;

public class RegisterActivity extends AppCompatActivity implements CallbackListener, View.OnClickListener{

    private SharedPreferences registerPrefs;
    private EditText mUserName;
    private EditText mPassword;
    private Button mSubmitButton;
    private EditText mContact;
    private Spinner mClassChoices;
    private Spinner mSchoolChoices;

    private ArrayList<String> mSchoolList = new ArrayList<>();
    private ArrayList<String> mClassList = new ArrayList<>();
    private HashMap<String, String> mUserData = new HashMap<>();
    private ArrayList<School> mSchools = new ArrayList<>();
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

        }

        @Override
        public void onVerificationFailed(FirebaseException e) {

        }
    };


    private String mUser;
    private CallbackListener mListener;
    private Context mContext;
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_layout);

        mListener = this;
        mContext = this;
        mActivity = this;

        mUserName = (EditText) findViewById(R.id.register_username_edittext);
        mPassword = (EditText) findViewById(R.id.register_password_edittext);
        mContact = (EditText) findViewById(R.id.register_contact_edittext);
        mSubmitButton = (Button) findViewById(R.id.submit_button);

        Intent intent = getIntent();
        //registerPrefs = getApplicationContext().getSharedPreferences(intent.getStringExtra("testKey"), MODE_PRIVATE);
        mSubmitButton.setOnClickListener(this);

        Spinner mUserChoices = (Spinner) findViewById(R.id.user_spinner);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.user_array, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mUserChoices.setAdapter(adapter2);
        mUserChoices.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mUser = parent.getItemAtPosition(position).toString();
                Log.e("role", mUser);
                mUserData.put("role", mUser);
                //Log.v("role", mUser);
                if (mUser.equals("Organization")) {
                    mSchoolChoices.setEnabled(false);
                    mClassChoices.setEnabled(false);
                } else if (mUser.equals("Cook")) {
                    mClassChoices.setEnabled(false);
                } else if (mUser.equals("Teacher")) {
                    mSchoolChoices.setEnabled(true);
                    mClassChoices.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mClassChoices = (Spinner) findViewById(R.id.class_spinner);
        mSchoolChoices = (Spinner) findViewById(R.id.school_spinner);
        HttpGetRequests task = new HttpGetRequests(GET_SCHOOLS, this, this);
        task.execute(REQUEST_ORG_DASHBOARD);
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.submit_button) {
            mUserData.put("username", mUserName.getText().toString());
            mUserData.put("password", mPassword.getText().toString());
            String contact = mContact.getText().toString().replaceAll("\\s+", "");
            if (contact.length() == 9)
                mUserData.put("contact", "+" + "254" + contact);
            else {
                mContact.setError("Valid number is required");
                mContact.requestFocus();
                return;
            }
            HttpPostRequests registerPost = new HttpPostRequests(mUserData, POST_NEW_USER, this, this);
            registerPost.execute(REQUEST_REGISTER_USER);
            /*
            SharedPreferences.Editor editor = registerPrefs.edit();
            if (mUserName.getText() != null && mPassword.getText() != null) {
                editor.putString("user" + mUserName.getText().toString(), mUserName.getText().toString());
                editor.putString("password" + mUserName.getText().toString(), mPassword.getText().toString());
                editor.apply();
                finish();
            } */
        }
    }


    private String getSchoolId(String schoolname) {
        for (School sch : mSchools) {
            if (schoolname.equals(sch.getSchoolName())){
                return sch.getSchoolID();
            }
        }
        return null;
    }


    @Override
    public void onCompletionHandler(boolean success, int requestcode, Object object) {
        if (success) {
            switch (requestcode) {
                case GET_SCHOOLS:
                    mSchools = (ArrayList<School>) object;
                    ArrayList<String> schoolNames = new ArrayList<>();
                    for (School sch : mSchools) {
                        schoolNames.add(sch.getSchoolName());
                    }
                    mSchoolList.addAll(schoolNames);
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item,
                                    mSchoolList);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            mSchoolChoices.setAdapter(adapter);
                            mSchoolChoices.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    String school = parent.getItemAtPosition(position).toString();
                                    mUserData.put("school", school);
                                    String schoolid = getSchoolId(school);
                                    HttpGetRequests task = new HttpGetRequests(GET_CLASSES, mListener, mContext);
                                    task.execute(REQUEST_CLASS_LIST + "/" + schoolid + "/" + "classes");
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }
                    });
                    break;
                case GET_CLASSES:
                    ArrayList<Class> classes = (ArrayList<Class>) object;
                    ArrayList<String> classNames = new ArrayList<>();
                    mClassList.clear();
                    for (Class cls : classes) {
                        classNames.add(cls.getName());
                    }
                    mClassList.addAll(classNames);
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ArrayAdapter<String> classadapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item,
                                    mClassList);
                            classadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            mClassChoices.setAdapter(classadapter);
                            mClassChoices.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    String classname = parent.getItemAtPosition(position).toString();
                                    mUserData.put("class", classname);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }
                    });
                    break;
                //need to set up phone authorization here;
                case POST_NEW_USER:
                    String userContact = mUserData.get("contact");
                    //Bundle contactInfo = new Bundle();
                    //contactInfo.putString("contactinfo", userContact);
                    Fragment successDialog = SuccessDialog.newInstance("Registration Successful!");
                    FragmentManager manager = getSupportFragmentManager();
                    manager.beginTransaction()
                            .add(successDialog, "successRegistration")
                            .commit();
                    Intent intent = new Intent(this, LoginActivity.class);
                    startActivity(intent);
                    break;
            }

        }
    }
}
