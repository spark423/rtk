package com.example.app.activities;


import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.app.R;
import com.example.app.asynctasks.HttpPutRequests;
import com.example.app.asynctasks.HttpDeleteRequests;
import com.example.app.interfaces.CallbackListener;
import com.example.app.models.StudentProfile;
import com.example.app.models.TeacherProfile;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import static com.example.app.util.Constants.PUT_STUDENT_PROFILE;
import static com.example.app.util.Constants.DELETE_STUDENT_PROFILE;
import static com.example.app.util.Constants.REQUEST_STUDENT_PROFILE;

//import org.parceler.Parcels;

public class EditStudentProfile extends AppCompatActivity implements View.OnClickListener, CallbackListener{

    private EditText mFirstName;
    private EditText mLastName;
    private EditText mGender;
    private EditText mBirthDay;
    private EditText mBirthMonth;
    private EditText mBirthYear;
    private EditText mGuardian;
    private EditText mTelephone;
    private EditText mNationalID;
    private EditText mAverageGrade;
    private EditText mShoesize;
    private Button mSave;

    private String mDatabaseId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_student_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mFirstName = (EditText) findViewById(R.id.edit_student_firstname);
        mLastName = (EditText) findViewById(R.id.edit_student_lastname);
        mGender = (EditText) findViewById(R.id.edit_student_gender);
        mBirthDay = (EditText) findViewById(R.id.edit_day);
        mBirthMonth = (EditText) findViewById(R.id.edit_month);
        mBirthYear = (EditText) findViewById(R.id.edit_yr);
        mGuardian = (EditText) findViewById(R.id.edit_student_guardian);
        mTelephone = (EditText) findViewById(R.id.edit_student_phone);
        mNationalID = (EditText) findViewById(R.id.edit_student_nationalID);
        mAverageGrade = (EditText) findViewById(R.id.edit_student_grade);
        mShoesize = (EditText) findViewById(R.id.edit_student_shoeSize);

        Gson gson = new Gson();
        String stdProfileString = getIntent().getStringExtra("CurrentStudentProfile");
        if (stdProfileString != null) {
            Type type = new TypeToken<StudentProfile>() {}.getType();
            StudentProfile input = gson.fromJson(stdProfileString, type);
            setProfile(input);
        }
        //StudentProfile input = Parcels.unwrap(getIntent().getParcelableExtra("CurrentStudentProfile"));
        //setProfile(input);

        mDatabaseId = getIntent().getStringExtra("Id");

        mSave = (Button) findViewById(R.id.save);
        mSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.save) {
            updateProfile();
        }
    }



    public void updateProfile() {
        String firstname = mFirstName.getText().toString();
        String lastname = mLastName.getText().toString();
        String gender = mGender.getText().toString();
        String birthdate = mBirthDay.getText().toString() + "/" + mBirthMonth.getText().toString()
                + "/" + mBirthYear.getText().toString();
        String guardian = mGuardian.getText().toString();
        String telephone = mTelephone.getText().toString();
        String nationalid = mNationalID.getText().toString();
        String grade = mAverageGrade.getText().toString();
        String shoesize = mShoesize.getText().toString();
        HashMap<String, String> studentProfile = new HashMap<>();
        studentProfile.put("firstname", firstname);
        studentProfile.put("lastname", lastname);
        studentProfile.put("gender", gender);
        studentProfile.put("dob", birthdate);
        studentProfile.put("guardian", guardian);
        studentProfile.put("contact", telephone);
        studentProfile.put("nationalid", nationalid);
        studentProfile.put("grade", grade);
        studentProfile.put("shoesize", shoesize);
        HttpPutRequests task = new HttpPutRequests(studentProfile, PUT_STUDENT_PROFILE, this, this);
        task.execute(REQUEST_STUDENT_PROFILE + "/" + mDatabaseId);
    }

    public void setProfile(StudentProfile curProfile) {
        mFirstName.setText(curProfile.getFirstName());
        mLastName.setText(curProfile.getLastName());
        mGender.setText(curProfile.getGender());
        String[] nums = curProfile.getDOB().split("/");
        mBirthDay.setText(nums[0]);
        mBirthMonth.setText(nums[1]);
        mBirthYear.setText(nums[2]);
        mGuardian.setText(curProfile.getGuardian());
        mTelephone.setText(curProfile.getTelephone());
        mNationalID.setText(curProfile.getNationalID());
        mAverageGrade.setText(curProfile.getAvegrade());
        mShoesize.setText(curProfile.getShoesize());
    }

    public void onCompletionHandler(boolean success, int requestcode, Object object) {
        if (success && requestcode == PUT_STUDENT_PROFILE) {
            StudentProfile mProfile = (StudentProfile) object;
            Gson gson = new Gson();
            Type type = new TypeToken<StudentProfile>() {}.getType();
            String updatedString = gson.toJson(mProfile, type);
            Intent returnIntent = new Intent();
            returnIntent.putExtra("UpdatedStudentProfile", updatedString);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }
}

