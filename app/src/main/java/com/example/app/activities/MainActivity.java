package com.example.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.example.app.R;
import com.example.app.asynctasks.HttpGetRequests;
import com.example.app.interfaces.CallbackListener;
import com.example.app.models.School;
import com.example.app.models.Student;
import com.example.app.util.BootingHandler;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

//import org.parceler.Parcels;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.example.app.util.Constants.GET_SCHOOLS;
import static com.example.app.util.Constants.GET_STUDENTLIST_VIEW;
import static com.example.app.util.Constants.GET_TOTAL_ATTENDANCE;
import static com.example.app.util.Constants.REQUEST_COOK_DASHBOARD;
import static com.example.app.util.Constants.REQUEST_ORG_DASHBOARD;
import static com.example.app.util.Constants.REQUEST_STUDENT_LIST;

public class MainActivity extends Activity implements CallbackListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences mSharedPreferences = getApplicationContext().getSharedPreferences("SHARED_PREFS_KEY", MODE_PRIVATE);
        if (mSharedPreferences.getString("authorization", null) == null) {
            //Log.v("test1", mSharedPreferences.getString("authorization", null));
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else {
            BootingHandler.initLauncher(this, this);
        }

        //HttpGetRequests task = new HttpGetRequests(GET_STUDENTLIST_VIEW, this, this);
        //task.execute(REQUEST_STUDENT_LIST);

        /*Intent newIntent = new Intent(this, LoginActivity.class);
        startActivity(newIntent);*/

        //HttpGetRequests task = new HttpGetRequests(GET_SCHOOLS, this, this);
        //task.execute(REQUEST_ORG_DASHBOARD);

        //HttpGetRequests task = new HttpGetRequests(GET_TOTAL_ATTENDANCE, this, this);
        //task.execute(REQUEST_COOK_DASHBOARD);
    }

    @Override
    public void onCompletionHandler(boolean success, int requestcode, Object object) {
        if (success) {
            switch (requestcode) {
                case GET_STUDENTLIST_VIEW:
                    ArrayList<Student> students = (ArrayList<Student>) object;
                    Intent intent = new Intent(this, ClassViewActivity.class);
                    Gson stdgson = new Gson();
                    Type stdtype = new TypeToken<ArrayList<School>>() {}.getType();
                    String studentListString = stdgson.toJson(students, stdtype);
                    intent.putExtra("Studentlist", studentListString);
                    startActivity(intent);
                    finish();
                    break;

                case GET_SCHOOLS:
                    ArrayList<School> schools = (ArrayList<School>) object;
                    Intent orgintent = new Intent(this, OrganizationDashboard.class);
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<School>>() {}.getType();
                    String schoolListString = gson.toJson(schools, type);
                    orgintent.putExtra("SchoolList", schoolListString);
                    startActivity(orgintent);
                    finish();
                    break;

                case GET_TOTAL_ATTENDANCE:
                    int mTotalAttendance = (int) object;
                    Intent cookintent = new Intent(this, FeedingDashboard.class);
                    Gson cookgson = new Gson();
                    Type cooktype = new TypeToken<Integer>() {}.getType();
                    String attendanceString = cookgson.toJson(mTotalAttendance, cooktype);
                    cookintent.putExtra("TotalAttendance", attendanceString);
                    startActivity(cookintent);
                    finish();

            }
        }
    }

}
